import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.files.inputmedia import *

import os.path
import threading
import time

chats_update = []

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)



def start(update, context):
    res = update.message.reply_photo(open('none.png', 'rb'))
    
    chats_update.append(res)
    
    
    update_frame()

def update_frame():
    while True:
        
        if not os.path.isfile('end'):
            continue
            
        for msg in chats_update:
            try:
                msg.edit_media(InputMediaAnimation(open('test.mp4', 'rb')))
            except:
                pass
                
        try: 
            if len(chats_update) > 0:
                os.remove("end")
        except:
                pass
                
        time.sleep(0.5)



def main():
    threading.Thread(target=update_frame).start()
    
    updater = Updater("Noooo token >.<", use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("iddqd", start))

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()