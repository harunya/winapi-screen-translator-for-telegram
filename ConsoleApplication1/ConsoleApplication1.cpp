﻿#include <iostream>
#include <windows.h>
#include <cstdio>
#include <tlhelp32.h>
#include <psapi.h>
#include <fstream>


DWORD FindProcessId(std::wstring& processName)
{
    PROCESSENTRY32 processInfo;
    processInfo.dwSize = sizeof(processInfo);

    HANDLE processesSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    if (processesSnapshot == INVALID_HANDLE_VALUE) {
        return 0;
    }

    Process32First(processesSnapshot, &processInfo);
    if (!processName.compare(processInfo.szExeFile))
    {
        CloseHandle(processesSnapshot);
        return processInfo.th32ProcessID;
    }

    while (Process32Next(processesSnapshot, &processInfo))
    {
        if (!processName.compare(processInfo.szExeFile))
        {
            CloseHandle(processesSnapshot);
            return processInfo.th32ProcessID;
        }
    }

    CloseHandle(processesSnapshot);
    return 0;
}

int main()
{
    std::string dllName("C:\\Users\\Haru\\source\\repos\\ConsoleApplication1\\Debug\\Inject.dll");
    std::wstring processName(L"DOSBox.exe");


    system("cd \"C://Program Files (x86)/DOSBox-0.74-3/\" & start DOSBox.exe");

    Sleep(3000);


    DWORD pid = FindProcessId(processName);

    HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

    LPVOID lpAdress = VirtualAllocEx(hProcess, NULL, dllName.length(), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
    BOOL WriteSuccess = WriteProcessMemory(hProcess, lpAdress, dllName.c_str(), dllName.length(), NULL);

    HMODULE k32 = LoadLibraryA("kernel32.dll");
    FARPROC LLproc = GetProcAddress(k32, "LoadLibraryA");
    FARPROC FLproc = GetProcAddress(k32, "FreeLibrary");


    CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)LLproc, lpAdress, NULL, NULL);


    DWORD lastError = GetLastError();
    

    std::cout << pid << std::endl;
    std::cout << hProcess << std::endl;
    std::cout << WriteSuccess << std::endl;
    std::cout << lastError << std::endl;

    

    //system("pause");
}
