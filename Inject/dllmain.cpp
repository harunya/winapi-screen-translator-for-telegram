﻿// dllmain.cpp : Определяет точку входа для приложения DLL.
#include "pch.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include <WinSock2.h>

std::string FILE_SAVE = "C://1/jpg_test.jpg";


std::wstring s2ws(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    std::wstring r(buf);
    delete[] buf;
    return r;
}

HWND g_HWND = NULL;
BOOL CALLBACK EnumWindowsProcMy(HWND hwnd, LPARAM lParam)
{
    DWORD lpdwProcessId;
    GetWindowThreadProcessId(hwnd, &lpdwProcessId);
    if (lpdwProcessId == lParam)
    {
        g_HWND = hwnd;
        return FALSE;
    }
    return TRUE;
}

inline bool file_exists(const std::string& name) {
    std::ifstream f(name.c_str());
    return f.good();
}


struct ScreenerArgs {
    DWORD processId;
    HWND hWnd;
    HDC hDC;
};

DWORD WINAPI screener(CONST LPVOID lpParam) {
    auto processId = GetCurrentProcessId();
    EnumWindows(EnumWindowsProcMy, processId);

    HWND hWnd = g_HWND;
    HDC hDC = GetDC(hWnd);

    int cur_frame = 0;

    while (true) {
        if (cur_frame >= 10 * 3) {
            cur_frame = 0;
        
            
            system("cd C://1/ & ffmpeg.exe -y -framerate 10 -i frames/%03d.jpg -vf format=yuv420p test.mp4");
            system("echo \"\" >> C:\\1\\end");
        }


        Sleep(1000/10);
        while (file_exists("C:\\1\\end")) { Sleep(10); } // wait to send


        RECT winRect = RECT();
        GetWindowRect(hWnd, &winRect);

        LONG winWidth = winRect.right - winRect.left;
        LONG winHeight = winRect.bottom - winRect.top + 1;


        HDC mbitmapDC = CreateCompatibleDC(hDC);
        HDC bitmapDC = CreateCompatibleDC(hDC);
        HBITMAP bitmap = CreateCompatibleBitmap(hDC, winWidth, winHeight);
        HBITMAP bitmap2 = CreateCompatibleBitmap(hDC, winWidth, winHeight);
        SelectObject(mbitmapDC, bitmap2);
        SelectObject(bitmapDC, bitmap);


        BitBlt(mbitmapDC, 0, 0, winWidth, winHeight, hDC, 0, 0, SRCCOPY);
        StretchBlt(bitmapDC, 0, winHeight, winWidth, -winHeight, mbitmapDC, 0, 0, winWidth, winHeight, SRCCOPY ); // Vert flip

        BITMAPINFO bitmapInfo = { 0 };
        bitmapInfo.bmiHeader.biSize = sizeof(bitmapInfo.bmiHeader);

        GetDIBits(bitmapDC, bitmap, 0, 0, NULL, &bitmapInfo, DIB_RGB_COLORS); // Get struct

        bitmapInfo.bmiHeader.biCompression = BI_RGB;
        bitmapInfo.bmiHeader.biBitCount = 32;
        bitmapInfo.bmiHeader.biHeight = abs(bitmapInfo.bmiHeader.biHeight);


        BYTE* bitBuffer = new BYTE[bitmapInfo.bmiHeader.biSizeImage];
        int result = GetDIBits(bitmapDC, bitmap, 0, bitmapInfo.bmiHeader.biHeight, bitBuffer, &bitmapInfo, DIB_RGB_COLORS);

        for (int i = 0; i < bitmapInfo.bmiHeader.biSizeImage; i+=4) {
            BYTE temp = bitBuffer[i];
            bitBuffer[i] = bitBuffer[i + 2];
            //bitBuffer[i+1] = 0;
            bitBuffer[i+2] = temp;
        }

        //SaveToFile(bitmap, "c:\\1\\test.bmp");

        char s[16];
        sprintf_s(s, "frames/%03d.jpg", cur_frame);


        stbi_write_jpg((std::string("C:\\1\\")+std::string(s)).c_str(), winWidth, winHeight, 4, bitBuffer, 10);

        //MessageBox(NULL, s2ws(std::to_string((int)res)).c_str(), L"Result", MB_OK);
        delete[] bitBuffer;

        DeleteObject(bitmap);
        DeleteObject(bitmap2);

        // Освобождаем DC
        ReleaseDC(hWnd, mbitmapDC);
        ReleaseDC(hWnd, bitmapDC);

        DeleteDC(mbitmapDC);
        DeleteDC(bitmapDC);

        


        cur_frame++;


    }

    ReleaseDC(hWnd, hDC);

    ExitThread(0);
    return 0;
    
}




BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    

    switch (ul_reason_for_call)
    {
        case DLL_PROCESS_ATTACH: {
                std::cout << "DLL injected!" << std::endl;

                CreateThread(NULL, 0, &screener, NULL, 0, NULL);
            }
            break;
        case DLL_THREAD_ATTACH:
            break;
        case DLL_THREAD_DETACH:
        case DLL_PROCESS_DETACH:
            break;
    }

    

    return TRUE;
}

